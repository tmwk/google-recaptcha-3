<?php

namespace TMWK\Recapcha3Bundle\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * ReCaptcha Validator.
 *
 */
class ReCaptchaValidator extends ConstraintValidator
{
    protected Request $request;
    protected string  $privateKey;

    /**
     * @param RequestStack $request
     * @param string       $privateKey
     */
    public function __construct(RequestStack $request, string $privateKey)
    {
        $this->request    = $request->getCurrentRequest();
        $this->privateKey = $privateKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ReCaptcha) {
            throw new UnexpectedTypeException($constraint, ReCaptcha::class);
        }

        /*if ($this->request->get('g-recaptcha-response', false)) {

            $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $this->privateKey . "&response=" . $this->request->get('g-recaptcha-response', false) . "&remoteip=" . $this->request->getClientIp()));

            if (!$response->success) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', $this->formatValue($value))
                    ->addViolation();
            }
        } else {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $this->formatValue($value))
                ->addViolation();
        }*/
    }

    /**
     * {@inheritdoc}
     */
    public function externValidate($value): bool
    {
        if ($this->request->get('g-recaptcha-response', false)) {

            $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $this->privateKey . "&response=" . $this->request->get('g-recaptcha-response', false) . "&remoteip=" . $this->request->getClientIp()));

            if ($response->success) {
                return true;
            }
        } else {
            return false;
        }

        return false;
    }
}
