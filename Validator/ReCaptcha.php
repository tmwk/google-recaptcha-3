<?php

namespace TMWK\Recapcha3Bundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * ReCaptcha Constraint.
 *
 * @Annotation
 *
 */
class ReCaptcha extends Constraint
{
    const IS_BLANK_ERROR = 'c1051bb4-d103-4f74-8988-acbcafc7fdc3';
    /** @var string */
	public string $message = 'Por favor, prueba que no eres un robot.';

    protected static $errorNames = [
        self::IS_BLANK_ERROR => 'IS_BLANK_ERROR',
    ];

	/** @return string */
	public function validatedBy()
    {
        return \get_class($this).'Validator';
	}
}