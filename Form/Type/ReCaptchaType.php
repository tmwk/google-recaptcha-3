<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: ReCaptchaType.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 23:25
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\Recapcha3Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\Locale\Locale;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ReCaptchaType extends AbstractType
{
    const JS_API_URL = 'https://www.google.com/recaptcha/api.js';

    /** @var  string */
    protected string $publicKey;

    /** @var  string */
    protected mixed $locale;

    public function __construct($publicKey, $locale = null)
    {
        if (null === $publicKey) {
            throw new InvalidConfigurationException('The parameters "public_key" must be configured.');
        }

        $this->publicKey = $publicKey;

        if (null !== $locale) {
            $this->locale = $locale;
        } else {
            $this->locale = Locale::getDefault();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars = array_replace(
            $view->vars,
            ['public_key'        => $this->publicKey,
             'lang'              => $this->locale,
             'js_api_url'        => self::JS_API_URL,
             'captcha_invisible' => $options['invisible'],
             'form_id'           => $options['form_id'],
             'form_error'        => $form->getParent()->getErrors()->getChildren() ? $form->getParent()->getErrors()->getChildren()->getMessage() : false,
            ]
        );


    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'invisible' => false,
                'form_id'   => '',

            ));
    }


    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'tmwk_recaptcha';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'tmwk_recaptcha';
    }
}
